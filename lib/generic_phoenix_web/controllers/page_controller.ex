defmodule GenericPhoenixWeb.PageController do
  use GenericPhoenixWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
