use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :generic_phoenix, GenericPhoenixWeb.Endpoint,
  secret_key_base: "AUGD2TW1zLp9PbO2TsDoqbBgWJtEyx0cYPV7PTH8c8rYuwYyrn0QAuiu5msf95sK"

# Configure your database
config :generic_phoenix, GenericPhoenix.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "generic_phoenix_prod",
  pool_size: 15
